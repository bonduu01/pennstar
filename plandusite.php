﻿<!DOCTYPE HTML>
<html>
<head>
  <title>PennStar Bank | Site Map</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css" />

<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />

<link rel="stylesheet" type="text/css" href="assets/css/custom_2.css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="assets/js/default.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>


<script src="assets/js/css3-mediaqueries.js" type="text/javascript"></script>
<script type='text/javascript' src="cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr.min.js"></script>




    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->
<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                            <li ><a href="index.php">Home</a></li>
        <li class="active"><a href="apropos.php">About Us</a></li>

        
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li><a href="emplois.php">Jobs</a></li>
         <li><a href="appelsdoffres.php">Procurement</a></li>

                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      


     
      


 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->
<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li><a href="apropos.php">About us</a></li>
            <li>Site Map</li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div id="divBox" class="container">
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<div class="thumb_nails">
<div class="caption">
<h1>Site Map</h1>
<table width="100%" border="0" cellspacing="0" class="table table-striped">
  <tr class="popover-content">
    <td align="left" valign="top" >&nbsp;</td>
    <td align="left" valign="top" >&nbsp;</td>
    <td class="style9 style7 style4 xl26">&nbsp;</td>
  </tr>
  <tr>
    <td width="31%" align="left" valign="top" ><span class="style1 style10"><strong>ABOUT PennStar Bank </strong></span></td>
    <td width="13%" align="left" valign="top" >&nbsp;</td>
    <td width="50%" class="xl26 style4 style7"><strong><span class="style11 style10">NEWS </span></strong></td>
  </tr>
  <tr>
    <td align="left" valign="top"><span class="style10"><a href="apropos.php">Vision</a> </span></td>
    <td align="left" valign="top">&nbsp;</td>
    <td class="style10"><a href="actualites.php">News and articles</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><span class="style10"><a href="apropos.php">Mission</a> </span> </td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="communiques.php">Press releases </a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="apropos.php">Corporate object of PennStar Bank</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td class="style10">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top" class="xl26 style4 style7"><strong><span class="style14 style10">PROJECTS AND OPERATIONS </span></strong></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="message_du_president.php" class="style10">Message from President</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">Interventions of PennStar Bank</td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="historique.php">Historique</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="investisseurs_positionnement.php">Strategic positioning of PennStar Bank</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="perspectives.php">Perspectives</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="investisseurs_actionnariat.php">Shareholding</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="siege.php">Headquaters</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="interventions_domaines.php">Areas of intervention</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="organisation.php">Organization</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="interventions_mecanisme.php">Mecanism of intervention</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="gouverneurs.php">The Board of Governors</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="investisseurs_conditions.php">Conditions for intervention </a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="administrateurs.php">The Board of Directors </a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="documents/download.php@filename=guide_for_inverstors_2008.pdf">Guide for investors</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="management.php">Management</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td class="style10 style4 style14"><strong>TOPICS </strong></td>
    </tr>
  <tr>
    <td class="style10 style4 style14"><strong>COUNTRIES</strong></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="themespauvrete.php">Poverty reduction</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="pays.php">Regional Context</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="themespublic.php">Public sector</a></td>
    </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td><a href="themesprive.php">Private sector</a></li></td>
    </tr>
  <tr>
    <td class="style10 style4 style14"><strong>OPPORTUNITIES </strong></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="themesenvironnement.php">Environmental and Social Management in Project Financing</a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a href="emplois.php">Employment</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a href="appelsdoffres.php">Procurement</a></td>
    <td  align="left" valign="top">&nbsp;</td>
    <td class="xl26 style4 style7"><strong><span class="style14 style10">SPECIAL FUNDS</span></strong></td>
    </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="fondstelelecom.php">Special Fund for Telecommunications and New Information Technology</a></td>
    </tr>
  <tr>
    <td align="left" valign="top" class="style10 style15 style12 style4 xl26"><strong>DOCUMENTS ET PUBLICATIONS </strong></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="fondsculturel.php">Guarantee Fund for Cultural Industries</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="publications.php">Publications</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><a href="fondsbiocarburant.php">African Bio-fuels and Renewable Energy Fund</a></td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="#">Media Contacts</a></td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td class="style10 style4 style14"><strong>CONTACT US</strong></td>
    <td align="left" valign="top" class="xl26 style4 style7 style9">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    </tr>
  <tr>
    <td align="left" valign="top"><a href="contact.php">Contacts</a></td>
    <td align="left" valign="top" class="xl26 style4 style7 style9">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a rel="facebox" href="admin/login.php" target="_blank">Staff email login</a></td>
    <td align="left" valign="top" class="xl26 style4 style7 style9">&nbsp;</td>
    <td class="xl26 style4 style7">&nbsp;</td>
    </tr>
</table>
<hr/>
</div>
</div>
</div>
</div>
</div>
			
</div>
<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>