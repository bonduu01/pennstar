<!DOCTYPE HTML>
<html>
<head>
<title>PennStar Bank | PennStar Bank Bank for Investment and Development</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank,  Investment Bank, Development, ">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="search.php" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->
<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                            <li ><a href="index.php">Home</a></li>
        <li class="active"><a href="apropos.php">About Us</a></li>

        
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li><a href="emplois.php">Jobs</a></li>
         <li><a href="appelsdoffres.php">Procurement</a></li>

                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      


     
      


 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->
<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li>About Us</li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div id="divBox" class="container">
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<ul class="thumb_nails">
<li class="span9">
<div class="thumb_nails">
<div class="caption">
 <h1>About PennStar Bank</h1>
<p>PennStar Bank Bank LLC. Authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority (Financial Services Register number: 122702).</p>
<p> PennStar Bank Bank LLC adheres to The Standards of Lending Practice which is monitored and enforced by The Lending Standards Board.  PennStar Bank Insurance Services Company Limited is authorised and regulated by the Financial Conduct Authority (Financial Services Register number: 312078).</p>
<p>PennStar Bank Bank LLC. Registered in England. Registered no. 1026167. PennStar Bank Insurance Services Company Limited. Registered in England. Registered no. 973765. Registered office for both: 1 Churchill Place, London E14 5HP. 'The Woolwich' and 'Woolwich' are trademarks and trading names of PennStar Bank Bank LLC. PennStar Bank Business is a trading name of PennStar Bank Bank LLC.</p>
<h2>Vision</h2>
<p>The vision  of PennStar Bank is to become the leading regional investment and development Bank in  Europe, an effective tool for poverty alleviation, wealth creation and job  promotion for the well-being of people in the region.</p>
<h2>Mission</h2>
<p>The  mission of the Bank is to foster the emergence of an economically strong,  industrialised and prosperous Europe with a fully integrated economic  system at regional and global levels in order to benefit from the opportunities  offered by globalisation.</p>


<h2>Corporate object</h2>
The Bank&rsquo;s corporate object is to:
<ul>
  <li>grant loans and guarantees for  financing investment projects and programmes relating to the economic and  social development of Member States;</li>
  <br />

<li>mobilize resources within and outside  the Community to finance the Bank&rsquo;s investment projects and programmes;<br />
</li>
 <br />
  <li>provide the technical assistance that  may be required within the Community to study, prepare, finance and implement  development projects and programmes;</li>
  <br />
  <li>receive and manage the portion of the  Community Levy resources earmarked for financing Community development  activities;</li><br />
  <li>manage any Community special funds  relevant to its corporate object;</li>
  <br />
  <li>carry out any commercial, industrial or  agricultural activity related to the Bank&rsquo;s corporate object or required for  the recovery of debts owed the Bank.</li>
</ul>
<br />
<p>Within the scope of its corporate object,  the PennStar Bank cooperates with national and sub-regional development organisations  operating within and outside the Community. Furthermore, the Bank cooperates  with other international organisations with similar aims and other institutions  involved in the development of the Community.</p>
<hr>
</div>
</div>
</li>
<!-- span3 -->
<li class="span3">
<br/><br/><br/><br/><br/>
  <div class="thumb_nails">
<div class="right_green"> 
  <aside>
        <nav>
          <ul>
          <br />
            <li><a href="documents/download.php@filename=PennStar Bankinbrief.pdf"><strong>PennStar Bank in Brief</strong> <span>(download)</span></a>
			<ul>
             <li><a href="historique.php">&nbsp;&nbsp;Background</a></li>
                <li><a href="siege.php">&nbsp;&nbsp;Headquaters</a></li>
                <li><a href="documents/download.php@filename=PennStar Bank_annual_report_2014_en.pdf">&nbsp;&nbsp;<strong>Annual Report 2014</strong></a></li>
                <li><a href="documents/download.php@filename=PennStar Bank_annual_report_2013_en.pdf">&nbsp;&nbsp;<strong>Annual Report 2013</strong></a></li>
                <li><a href="documents/download.php@filename=PennStar Bank_annual_report_2012_en.pdf">&nbsp;&nbsp;<strong>Annual Report 2012</strong></a></li>
                 <li><a href="documents/download.php@filename=PennStar Bank_annual_report_2011_en.pdf">&nbsp;&nbsp;<strong>Annual Report 2011</strong></a></li>
        	</ul>
            
        
<hr />
        <li><a href="organisation.php"><strong>Organization</strong></a>
        <ul>
             <li><a href="gouverneurs.php">&nbsp;&nbsp;Board of Governors</a></li>
          <li><a href="administrateurs.php">&nbsp;&nbsp;Board of Directors</a></li>
          <li><a href="management.php">&nbsp;&nbsp;Management</a></li>
        </ul>
        </li>
        </ul>
        </nav>
        <!-- /nav -->
        <!-- ########################################################################################## -->
      </aside>
</div>
</div>
</li>
            
            
            
            <!-- / span3 -->
</ul>
</div>
</div>
</div>
			
</div>
<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>