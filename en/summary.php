﻿<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>

<title>PennStar Bank | Account Summary Page</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

    <style type="text/css">
        .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #FAF7FA; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table tbody td { color: #7D7D7D; border-left: 1px solid #35992E;font-size: 12px;font-weight: bold; }.datagrid table tbody .alt td { background: #D3EBAE; color: #7D7D7D; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
        .style2
        {
            width: 114px;
        }
        .style3
        {
            width: 132px;
        }
        .style4
        {
            width: 170px;
        }
        .style5
        {
            width: 136px;
        }
        .style6
        {
            width: 98px;
        }
    </style>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->

<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
         
        <li ><a href="home.php">Profile</a></li>
        <li class="active"><a href="summary.php">Summary</a></li>
        <li><a href="fundTransfer.php">Fund Transfer</a></li>
        <li><a href="changePwd.php">Change Password</a></li>
        <li><a href="logoff.php">SignOut</a></li>
 

                            
                            
                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      




 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->

<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li>Resources</li>
            <li>Employment</li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div class="row"><br> </div>
<div id="divBox" class="container">
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<ul class="thumb_nails">
<li class="span9">
<div class="thumb_nails">
<div class="caption">

<br />
<?php 
							if (isset($_SESSION['trackme'])) 
							{
							// Grab the track data from the POST
							$track = $_SESSION['trackme'];

						$con = mysql_connect('localhost','u905291877_proot',"Batsignal30$");
						if (!$con)
						  {
						  die('Could not connect: ' . mysql_error());
						  }
						
						
						mysql_select_db("testdbold", $con);
						
						$member_id = $_SESSION['trackme'];
						$result = mysql_query("SELECT * FROM user_data1 WHERE track_number = '$member_id'");
						$row = mysql_fetch_array($result);
						if (empty($result))
						{
						echo '<p class="error">Please enter the right information  </p>';
						
						}
						else
						{
						$id=$row["id"];
 						$fname=$row["fname"];
						$lname=$row["lname"];
						$filename=$row["filename"];
                        $track_number=$row["track_number"];
						$fundTrans=$row["fundTrans"];
                        $dob=$row["dob"];
                        $email=$row["email"];
                        $country=$row["country"];
                        $gender=$row["gender"];
						$recipientName=$row["recipientName"];
						$address=$row["address"];
						$bankInfo=$row["bankInfo"];
						$amt=$row["amt"];
						$status=$row["status"];
						mysql_close($con);
						}
						}
						?>	
						Welcome back <?php echo $fundTrans; ?><br />
    <br />
<div class="datagrid">
<table>
<tbody>

<table>
<thead><tr><th class="style4">Date</th><th class="style5">Description</th>
    <th class="style2">Ref.</th><th class="style6">Withdrawals</th>
    <th class="style3">Deposits</th><th>Balance</th></tr></thead>
<tbody>
    <?php
	$conn = mysql_connect('localhost','u905291877_proot',"Batsignal30$");
						if (!$conn)
						  {
						  die('Could not connect: ' . mysql_error());
						  }
						mysql_select_db("testdbold", $conn);
						$member_id = $_SESSION['trackme'];
						$resultd = mysql_query("select created,description,reference,amt_debited,Final_amt FROM debitbarc WHERE track_number = '$member_id' order by created desc");
						
						$resultc = mysql_query("SELECT created,description,reference,amt_credited,Final_amt FROM creditbarc WHERE track_number = '$member_id' order by created desc");
						
						$rownum = mysql_num_rows($resultd);
						$rownumc = mysql_num_rows($resultc);
						if (empty($resultd))
						{
						echo '<p class="error"></p>';
						}
						else
						{
				
						for ($i = 0; $i < $rownum; $i++) {
								     if (!(empty($row_d["created"])) || !(isset($row_d["created"])) || !(empty($row_d["amt_debited"])) || !(isset($row_d["amt_debited"]))) 
								{
									$row_d = mysql_fetch_array($resultd);
									echo '<tr class="alt">';
									echo '<td class="style12"><b>'.$row_d["created"].'</td>';
									echo '<td class="style12">'.$row_d["description"].'</td>';
									echo '<td class="style12">'.$row_d["reference"].'</td>';
									echo '<td class="style12">'."$".$row_d["amt_debited"].'</td>';
									echo '<td class="style12">'."".'</td>';
									echo '<td class="style12">'."$".$row_d["Final_amt"].'</td>';
									echo '</tr>';
		                        }	 
                        }
						}
						if (empty($resultc))
						{
						echo '<p class="error"></p>';
						}
						else
						{
						for ($j = 0; $j < $rownumc; $j++) {
								if (!(empty($row_c["created"])) || !(isset($row_c["created"])) || !(empty($row_c["amt_credited"])) || !(isset($row_c["amt_credited"]))) 
								{
									$row_c = mysql_fetch_array($resultc);
									echo '<tr class="alt">';
									echo '<td ><b>'.$row_c["created"].'</td>';
									echo '<td>'.$row_c["description"].'</td>';
									echo '<td >'.$row_c["reference"].'</td>';
									echo '<td>'."".'</td>';
									echo '<td ">'."$".$row_c["amt_credited"].'</td>';
									echo '<td >'."$".$row_c["Final_amt"].'</td>';
									echo '</tr>';
				
		                        }	 
                        }
						}
						
						mysql_close($conn);
	 ?>
	 <tr class="alt">
<td class="style4"><b><?php $dt = new DateTime(); echo $dt->format('Y-m-d H:i:s'); ?></b></td>
<td class="style5"></td>
<td class="style2"></td>
<td class="style6"></td>
<td class="style3"></td>
<td></td>
</tr>
<tr class="alt">
<td class="style4"><b><?php $dt = new DateTime(); echo $dt->format('Y-m-d H:i:s'); ?></b></td>
<td class="style5"></td>
<td class="style2"></td>
<td class="style6"></td>
<td align ="right" class="style3">Balance: </td>
<td align ="left"><b><?php $usd="$"; echo $usd.$amt; ?></b></td>
</tr>
</tbody>
</table>
</div>
<!-- ##################################      END Here ########################### -->
</div>
</div>
</li>
<!-- span3 -->
  </ul>
</div>
</div>
</div>
	<hr/>		
</div>
			

<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
		    $('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>