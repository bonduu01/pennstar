﻿<?php
// Start a session for error reporting
session_start();

// Call our connection file
require("includes/conn.php");

// Check to see if the type of file uploaded is a valid image type
function is_valid_type($file)
{
	// This is an array that holds all the valid image MIME types
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif", "image/png");

	if (in_array($file['type'], $valid_types))
		return 1;
	return 0;
}

// Just a short function that prints out the contents of an array in a manner that's easy to read
// I used this function during debugging but it serves no purpose at run time for this example
function showContents($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

// Set some constants

// This variable is the path to the image folder where all the images are going to be stored
// Note that there is a trailing forward slash
$TARGET_PATH = "images/";

// Get our POSTed variables
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$image = $_FILES['image'];
$track_number = $_POST['track_number'];
$fundTrans = $_POST['fundTrans'];
$dob = $_POST['dob'];
$email = $_POST['email'];
$country = $_POST['country'];
$gender = $_POST['gender'];
$recipientName = $_POST['recipientName'];
$address = $_POST['address'];
$bankInfo = $_POST['bankInfo'];
$amt = $_POST['amt'];
$status = $_POST['status'];
$username = $_POST['username'];
$password = $_POST['password'];
$cot_number = $_POST['cot_number'];
$tax_number = $_POST['tax_number'];
$yellow_number=$_POST['yellow_number'];
$suspend = $_POST['suspend'];

// Sanitize our inputs
$fname = mysql_real_escape_string($fname);
$lname = mysql_real_escape_string($lname);
$image['name'] = mysql_real_escape_string($image['name']);
$track_number = mysql_real_escape_string($track_number);
$fundTrans = mysql_real_escape_string($fundTrans);
$dob = mysql_real_escape_string($dob);
$email = mysql_real_escape_string($email);
$country = mysql_real_escape_string($country);
$gender = mysql_real_escape_string($gender);
$recipientName = mysql_real_escape_string($recipientName);
$address = mysql_real_escape_string($address);
$bankInfo = mysql_real_escape_string($bankInfo);
$amt = mysql_real_escape_string($amt);
$status = mysql_real_escape_string($status);
$username = mysql_real_escape_string($username);
$password = mysql_real_escape_string($password);
$cot_number =  mysql_real_escape_string($cot_number);
$tax_number =  mysql_real_escape_string($tax_number);
$yellow_number = mysql_real_escape_string($yellow_number);
$suspend = mysql_real_escape_string($suspend);

// Build our target path full string.  This is where the file will be moved do
// i.e.  images/picture.jpg
$TARGET_PATH .= $image['name'];

// Make sure all the fields from the form have inputs
if ( $fname == "" || $lname == "" || $image['name'] == "" )
{
	$_SESSION['error'] = "All fields are required";
	header("Location: index.php");
	exit;
}


// Check to make sure that our file is actually an image
// You check the file type instead of the extension because the extension can easily be faked
if (!is_valid_type($image))
{
	$_SESSION['error'] = "You must upload a jpeg, gif, bmp or a png";
	header("Location: index.php");
	exit;
}

// Here we check to see if a file with that name already exists
// You could get past filename problems by appending a timestamp to the filename and then continuing
//if (file_exists($TARGET_PATH))
//{
//	$_SESSION['error'] = "A file with that name already exists";
//	header("Location: index.php");
//	exit;
//}

// Lets attempt to move the file from its temporary directory to its new home
if (move_uploaded_file($image['tmp_name'], $TARGET_PATH))
{
	// NOTE: This is where a lot of people make mistakes.
	// We are *not* putting the image into the database; we are putting a reference to the file's location on the server
$sql = "insert into user_data1 (fname, lname, filename,track_number,fundTrans,dob,email,country,gender,recipientName,address,bankInfo,amt,status,username,password,cot_number,tax_number,yellow_code) 
  values ('$fname', '$lname','" . $image['name'] . "','$track_number','$fundTrans','$dob','$email','$country','$gender','$recipientName','$address','$bankInfo','$amt','$status','$username','$password','$cot_number','$tax_number','$yellow_number')";
	$result = mysql_query($sql) or die ("Could not insert data into DB: " . mysql_error());
	header("Location: record.php");
	exit;
}
else
{
	// A common cause of file moving failures is because of bad permissions on the directory attempting to be written to
	// Make sure you chmod the directory to be writeable
	$_SESSION['error'] = "Could not upload file.  Check read/write persmissions on the directory";
	header("Location: index.php");
	exit;
}
?>
