<!DOCTYPE HTML>
<html>
<head>
<title>PennStar Bank | Regional Outlook</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->
<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
         
  <li ><a href="index.php">Home</a></li>
        <li><a href="apropos.php">About Us</a></li>

        <li class="active"><a href="pays.php">Countries</a></li>
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li><a href="emplois.php">Jobs</a></li>
         <li><a href="appelsdoffres.php">Procurement</a></li>

                            
                            
                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      




 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->
<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li><a href="pays.php">Regional Economy</a></li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div id="divBox" class="container">
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<ul class="thumb_nails">
<li class="span9">
<div class="thumb_nails">
<div class="caption">
<h1> Regional Outlook</h1>
under construction

   <br />
  <p>&nbsp;</p>
  <table class="table table-striped" width="100%" border="0">
   <tr class="popover-content">
    <td valign="top"><strong>COUNTRIES</strong></td>
    <td align="center" valign="top"><p align="center"><strong>Demography</strong></p></td>
    <td align="center" valign="top"><p align="center"><strong>Geography</strong></p></td>
    <td align="center" valign="top"><p align="center"><strong>Economy</strong></p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Benin</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">3,0%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">2,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,9%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Burkina Faso</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">5,5%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">5,4%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">2,6%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Cote d&rsquo;Ivoire</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">7,4%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">6,3%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">6,2%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Guinee Bissau</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,5%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,7%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,2%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Mali</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">5,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p>24,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">2,6%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Niger</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">5,1%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p>24,8%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,6%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Senegal</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">4,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">3,8%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">3,5%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Togo</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">2,0%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,1%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,9%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p><strong>U</strong><strong>EM</strong><strong>OA</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>32</strong><strong>,</strong><strong>7</strong><strong>%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p><strong>68</strong><strong>,</strong><strong>6</strong><strong>%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p><strong>19</strong><strong>,</strong><strong>5</strong><strong>%</strong></p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Gambie</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,6%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,2%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Ghana</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">7,9%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">4,7%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">9,7%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Guinee</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">3,4%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">4,8%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,4%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Liberia</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,3%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">2,2%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,4%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Nigeria</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">52,0%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p>18,1%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">67,3%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Sierra Leone</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,9%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">1,4%</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center">0,9%</p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p><strong>ZM</strong><strong>AO</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>67</strong><strong>,</strong><strong>1</strong><strong>%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p><strong>31</strong><strong>,</strong><strong>3</strong><strong>%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p><strong>80</strong><strong>,</strong><strong>0</strong><strong>%</strong></p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p>Cap Vert</p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>0,2%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>0,1%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>0,5%</strong></p></td>
  </tr>
  <tr>
    <td valign="top"><p>&nbsp;</p>
      <p><strong>CEDEAO</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>100%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p><strong>100%</strong></p></td>
    <td align="center" valign="top"><p>&nbsp;</p>
      <p align="center"><strong>100%</strong></p></td>
  </tr>
</table>
   <br />
  <p>&nbsp;</p>

<!-- ##################################      END Here ########################### -->


</div>
</div>
</li>
<!-- span3 -->
 <li class="span3">
<br/><br/><br/><br/><br/>
 <div class="thumb_nails">
<div class="right_#06B1EF"> 
      <aside>
        <!-- /nav -->
        <nav>
          <ul>
          <br />
             <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Country Files</strong>
             <hr />
        <ul>
    <li><a href="pays_details.php@recordID=1">&nbsp;&nbsp;Benin</a></li>
    <li><a href="pays_details.php@recordID=2">&nbsp;&nbsp;Burkina Faso</a></li>
    <li><a href="pays_details.php@recordID=3">&nbsp;&nbsp;Cabo Verde</a></li>
    <li><a href="pays_details.php@recordID=4">&nbsp;&nbsp;Cote&nbsp;d&rsquo;Ivoire</a></li>
    <li><a href="pays_details.php@recordID=5">&nbsp;&nbsp;Gambia</a></li>
    <li><a href="pays_details.php@recordID=6">&nbsp;&nbsp;Ghana</a></li>
    <li><a href="pays_details.php@recordID=7">&nbsp;&nbsp;Guinea</a></li>
    <li><a href="pays_details.php@recordID=8">&nbsp;&nbsp;Guinea&nbsp;Bissau</a></li>
    <li><a href="pays_details.php@recordID=9">&nbsp;&nbsp;Liberia</a></li>
    <li><a href="pays_details.php@recordID=10">&nbsp;&nbsp;Mali</a></li>
    <li><a href="pays_details.php@recordID=11">&nbsp;&nbsp;Niger</a></li>
    <li><a href="pays_details.php@recordID=12">&nbsp;&nbsp;Nigeria</a></li>
    <li><a href="pays_details.php@recordID=13">&nbsp;&nbsp;Senegal</a></li>
    <li><a href="pays_details.php@recordID=14">&nbsp;&nbsp;Sierra Leone</a></li>
    <li><a href="pays_details.php@recordID=15">&nbsp;&nbsp;Togo</a></li>
        </ul>
        </li>
        </ul>
        </nav>
        <!-- ########################################################################################## -->
      </aside></div>
</div>
</li>
 


<!-- / span3 -->
</ul>
</div>
</div>
</div>
			
</div>
<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>