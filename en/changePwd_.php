﻿<?php
session_start();
?>
<html ng-app="validation">
  <head>
    <script src="https://code.angularjs.org/1.3.0-rc.2/angular.js"></script>
    <script src="https://code.angularjs.org/1.3.0-rc.2/angular-messages.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="js/style.css" />
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.css" />-->
    <style type="text/css">
      .style1
      {
      color: #02205D;
      text-align: center;
      }
      .form-control
      {
      font-weight: 700;
      }
    </style>

  </head>
  <script language=javascript>
      function validatePassword() {
          var currentPassword, newPassword, confirmPassword, output = true;

          currentPassword = document.frmChange.currentPassword;
          newPassword = document.frmChange.newPassword;
          confirmPassword = document.frmChange.confirmPassword;

          if (!currentPassword.value) {
              currentPassword.focus();
              document.getElementById("currentPassword").innerHTML = "required";
              output = false;
          }
          else if (!newPassword.value) {
              newPassword.focus();
              document.getElementById("newPassword").innerHTML = "required";
              output = false;
          }
          else if (!confirmPassword.value) {
              confirmPassword.focus();
              document.getElementById("confirmPassword").innerHTML = "required";
              output = false;
          }
          if (newPassword.value != confirmPassword.value) {
              newPassword.value = "";
              confirmPassword.value = "";
              newPassword.focus();
              document.getElementById("confirmPassword").innerHTML = "Password mismatch";
              output = false;
          }
          return output;
      }
</script>
 <img src="images/PennStar Bank-logo-Europe.png" alt ="bidc logo" width="123" height="120" />
  <?php 
							if (isset($_SESSION['trackme'])) 
							{
							// Grab the track data from the POST
							$track = $_SESSION['trackme'];
						$con = mysql_connect('localhost','u905291877_proot',"Batsignal30$");
						if (!$con)
						  {
						  die('Could not connect: ' . mysql_error());
						  }
						
						
						mysql_select_db("testdbold", $con);
						
						$member_id = $_SESSION['trackme'];
						$result = mysql_query("SELECT * FROM user_data1 WHERE track_number = '$member_id'");
						$row = mysql_fetch_array($result);
						if (empty($result))
						{
						echo '<p class="error">Please enter the right information  </p>';
						}
						else
						{
						$id=$row["id"];
 						$fname=$row["fname"];
						$lname=$row["lname"];
						$filename=$row["filename"];
                        $track_number=$row["track_number"];
						$fundTrans=$row["fundTrans"];
                        $dob=$row["dob"];
                        $email=$row["email"];
                        $country=$row["country"];
                        $gender=$row["gender"];
						$recipientName=$row["recipientName"];
						$address=$row["address"];
						$bankInfo=$row["bankInfo"];
						$amt=$row["amt"];
						$status=$row["status"];
						$password=$row["password"];
						mysql_close($con);
						}
						}
						?>
  <body class="container" ng-controller="RegistrationController as registration">
    <!-- <form action="changePassword.php" method="post" novalidate="" data-ng-submit="submitForm()" name="myForm">
    <div>
      <h2 class="style1">Authorised Login</h2>

    </div>
    <p>
      <table width="100%">
        <tr>
          <td align="left" valign="top" id="Td3" class="style1">
            <strong></strong>
          </td>
          <td width="280" align="left" valign="top">
            <input name="username" type="text" size="40" style="color: #02205D; text-align: left;" 
                   readonly="true" value="Account No:<?php echo $track_number; ?> "/>
          </td>
        </tr>
        <tr>
          <td>
            <span class="help-block" data-ng-show="myForm.oldpwd.$error.required">Required*</span>
          </td>
          <td width="280" align="left" valign="top">
            <input name="oldpwd" id="oldpwd" type="password" placeholder="Old Password"
                size="40" style="color: #02205D; text-align: left;" required="" data-ng-model="Password" />
          </td>

        </tr>
        <tr>
          <td>
            <span class="help-block" data-ng-show="myForm.newpwd.$error.required">Required*</span>
          </td>
          <td align="left" valign="top">
            <input name="newpwd" id="newpwd" type="password" size="40" placeholder="New Password"
                style="color: #02205D; text-align: left;" />
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" id="Td2" class="style1">
            <strong></strong>
          </td>
          <td align="left" valign="top">
            <input name="newpwd1" type="password" size="40" placeholder="Confirm New Password"
                style="color: #02205D; text-align: left;" />
          </td>
        </tr>
      </table>
      <div style="text-align: center">
        <input type="submit" name="Submit" value="Submit"
                style="height: 27px; color: #02205D; width: 77px; font-weight: 700" />
      </div>
    </form>
    -->
    <h2 class="style1">Change Password</h2>
    <h3 style="text-align: center; color: #990000">Password Change Failed..please retry or contact customer support</h3> 
<form name="frmChange" method="post" action="changePassword.php" onSubmit="return validatePassword()">
<div style="width:1000px;">
<div class="message"><?php if(isset($message)) { echo $message; } ?></div>
<table border="0" cellpadding="10" cellspacing="0" width="500" align="center" class="tblSaveForm">
<tr>
<td width="40%"><label>Account Number</label></td>
<td width="60%">
<?php echo $track_number; ?>
</td>
</tr>
<tr>
<td width="40%"><label>Current Password</label></td>
<td width="60%"><input type="password" name="currentPassword" class="txtField" value = "" /><span id="currentPassword"  class="required"></span></td>
</tr>
<tr>
<td><label>New Password</label></td>
<td><input type="password" name="newPassword" class="txtField"/><span id="newPassword" class="required"></span></td>
</tr>
<td><label>Confirm Password</label></td>
<td><input type="password" name="confirmPassword" class="txtField"/><span id="confirmPassword" class="required"></span></td>
</tr>
<tr>
<td colspan="2"><input type="submit" name="submit" value="Submit" class="btnSubmit"></td>
</tr>
</table>
    </form>
    <br />
  </body>
</html>
