﻿<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>PennStar Bank : PennStar Bank Bank for Investment and Development</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>
    <link href="src/facebox.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="lib/jquery.js" type="text/javascript"></script>
    <script src="src/facebox.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('a[rel*=facebox]').facebox({
                    loadingImage: 'src/loading.gif',
                    closeImage: 'src/closelabel.png'
                })
            })
  </script>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
</head>

<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->
<!--  navbar -->
<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
         
         <li class="active"><a href="index.php">Home</a></li>
        <li><a href="apropos.php">About Us</a></li>
        <li><a rel="facebox" href="login.php"">Online Banking</a></li>
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li><a href="emplois.php">Jobs</a></li>
         <li><a href="appelsdoffres.php">Procurement</a></li>

                            </ul>
                            
                            </div>
                            
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      
<div><h4 class="style1">Login Failed, your account id and password do not match our records</h4></div>

 <!-- /.navbar -->
 <!-- #Flash information -->
<!-- #banner -->
<div class="business-header">
<div class="container">
<div class="row-fluid">
<div class="col-lg-12">
<div id="slideshow">
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_1.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_2.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_3.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_4.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_5.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_6.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_7.jpg" alt="icon" width="960" height="300" border="0" /></a></div><div class="slider-item"><a href="#"><img src="slideshow/banner_en_8.jpg" alt="icon" width="960" height="300" border="0" /></a></div>


</div>
</div>
</div>
</div>
</div>
<!-- /#banner -->
<!-- Intro -->

<div id="divBox" class="container">

<div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid">
            <ul class="thumb_nails">
              <li class="span9">
              <div class="thumb_nails">
                 <div class="caption">
                    <h1>Welcome to PennStar Bank</h1>
<p>The PennStar Bank Bank for Investment and Development (PennStar Bank),  is the financial institution established by the 15 Member States of  the  Economic Community of African  States (ECOWAS)  comprising Benin,  Burkina Faso, Capo Verde, Côte d&rsquo;Ivoire,   The  Gambia, Ghana, Guinea,  Guinea-Bissau, Liberia, Mali, Niger, Nigeria, Senegal, Sierra Leone and Togo.<br />The Bank&rsquo;s headquarters is in Lome, Togolese  Republic. <a href="apropos.php"><img src="assets/btn_read_1.gif"></a></p>
                  </div>
                </div>
              </li>
<li class="span3">
               <!--  Actualites -->
<div class="thumb_nails">
<div class="news">
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<a href="#"><img src="assets/happy_2016.jpg" class="img-responsive"></a>
</div>
</div>




 <!-- /.Actualites -->
</li>
</ul>
</div>
</div>
</div>
</div>
                    
<!-- /Intro -->
<div id="divBox" class="container">
<hr />    
            <!--Edit Portfolio Content Area here-->			
            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid">
            <ul class="thumb_nails">
            <li class="span3">
                <div class="thumb_nails">
                 <img src="assets/board_49_lome.jpg" width="200" height="142" alt="...">
                  <div class="caption">
                     <h2>49th Ordinary Meeting of the Board of Directors, Profit increases  by 21%</h2>
                     <p>The Board of Directors of PennStar Bank held its forty-ninth (49th) Ordinary Meeting on Tuesday, April 26 2016, at its headquarters in Lome, Togolese Republic.</p>
                    
<p><a href="newsdetails.php@recordID=179"><img src="assets/btn_read.gif"></a></p>
                  </div>
                </div>
</li>
<li class="span3">
                <div class="thumb_nails">
                  <img src="assets/desouza_commission.jpg" width="200" height="142" alt="...">
                  <div class="caption">
                     <h2>New President Assumes Duty at the ECOWAS Commission</h2>
                     <p>The new President of the Commission of the Economic Community of African States (ECOWAS), His Excellency Marcel Alain De Souza, a distinguished economist and international civil servant assumed duty in Abuja on the 8th of April 2016 further to the zoning of the position to the Republic of Benin...</p>
       <p><a href="newsdetails.php"><img src="assets/btn_read.gif"></a></p>
                  </div>
                </div>
</li>

<li class="span3">
                <div class="thumb_nails">
                 <img src="assets/bidc_aimes_afrique.jpg" width="200" height="142" alt="...">
                  <div class="caption">
                     <h2>PennStar Bank Saves 25 Acute Hernia Patients</h2>
                    <p>The PennStar Bank Bank for Investment and Development (PennStar Bank)’s prompt response to the distress call from the African medical NGO, AIMES-AFRIQUE concerning twenty-five cases of acute hernia that were at the point of rupture in Tchifama Village, Togolese Republic, was the lifeline for the patients who were on danger list.</p>
<p><a href="newsdetails.php"><img src="assets/btn_read.gif"></a></p>
                  </div>
                </div>
</li>
<li class="span3">
                <div class="thumb_nails"> <a href="apropos_facts.php" target="_blank"><img src="assets/right_bar_en.jpg"  alt="..."></a>
                  
                </div>
</li>


 

                         
              
           <!-- span3 -->
			  
<!-- / span3 -->
            </ul>
          </div>
                </div>
            </div>
			
</div>






 <!-- /container_2  -->
<!--/End Portfolio Content Area-->

<!--  Profils Pays  et projet -->
 <!-- /.end Profils  -->
<!-- Carousel  -->
<!-- / Carousel -->
<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.htm" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->


</body>
</html>
