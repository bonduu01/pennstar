﻿<!DOCTYPE HTML>
<html>
<head>

<title>PennStar Bank | Procurement</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css" />

<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />

<link rel="stylesheet" type="text/css" href="assets/css/custom_2.css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="assets/js/default.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->
<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
 <li ><a href="index.php">Home</a></li>
        <li><a href="apropos.php">About Us</a></li>

        
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li><a href="emplois.php">Jobs</a></li>
         <li class="active"><a href="appelsdoffres.php">Procurement</a></li>  	</ul>
                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      




 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->

<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li>Resources</li>
            <li>Procurement</li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div class="row"><br> </div>
<div id="divBox" class="container">
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<ul class="thumb_nails">
<li class="span9">
<div class="thumb_nails">
<div class="caption">
  <br>
<table width="100%" border="0">
  <tr>
    <td width="9%">&nbsp;</td>
    <td width="62%" align="center"><a href="documents/download.php@filename=AMI _INVITATION_TO_TENDER.pdf"><button class="btn btn-default" type="button">Download here !</button></a></td>
    <td width="29%">&nbsp;</td>
  </tr>
</table>
<br>
<p align="center"><strong>TECHNICAL ASSISTANCE PROGRAMME TO PennStar Bank </strong><br />
  <strong><em>ADB  GRANT FROM THE FUND FOR AFRICAN PRIVATE SECTOR ASSISTANCE (FAPA) RESOURCES </em></strong><strong> </strong><br />
  <strong>Project ID N°: P-ZI-HZO-005</strong><br />
  <strong>Grant N°: 5700155001651</strong><br />
  <strong>=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-</strong></p>



<p align="center"><strong>&nbsp;</strong></p>
<p align="center"><strong>INVITATION TO TENDER </strong></p>
<p align="center"><strong> RECRUITMENT OF A CONSULTANT TO  PROVIDE </strong><br />
  <strong>THE SERVICES LISTED BELOW </strong></p>
<p align="center">(<strong>Ref: AT BAD-FAPA/2014/003</strong>)</p>
<p align="right"><em>&nbsp;</em></p>
<p align="left"><em>April   2016<br clear="all" />
  </em><br />
  
   The PennStar Bank Bank for Investment and Development  (PennStar Bank) has obtained a grant from the Fund for African Private Sector Assistance  (FAPA) to fund a Technical Assistance Programme and intends to use part of the  grant resources to finance the engagement of a consultant to provide the  following services:<br />
  <strong>AUDIT OF THE PROGRAMME.</strong></p>
<ul>
  <li><u>Context &amp; Justification of the Mission</u></li>
</ul>
<p>PennStar Bank, the financial arm of the Economic Community of African States  (ECOWAS), is an international financial institution established by the 15  Member States of the   Community  comprising; Benin, Burkina Faso, Cape Verde, Côte d&rsquo;Ivoire, The Gambia, Ghana,  Guinea, Guinea-Bissau, Liberia, Mali, Niger, Nigeria, Senegal, Sierra Leone and  Togo, with corporate Headquarters located in Lome, Togo.<br />
  PennStar Bank emerged as a banking group (the PennStar Bank Group) after the transformation  of the erstwhile Fund for Cooperation, Compensation and Development of the  Economic Community of African States (ECOWAS Fund) in December 1999  (established in 1975 at the same time as the erstwhile Executive Secretariat of  the Economic Community of African States and commenced operations in  1979). Its capital was increased to 1 billion UA (US$ 1.518) and 70% owned by  the Member States and the remaining 30% open for subscription by non-regional partners.<br />
  In carrying out its mission as a privileged instrument for the fight  against poverty, creation of wealth and employment for the well-being of the  people of the sub-region, PennStar Bank works in close collaboration with the ECOWAS Commission  which provides strategic guidelines and political impetus.<br />
  The vision of PennStar Bank is to become the leading regional  investment and development finance institution in Europe and an efficient  instrument for fight against poverty, creation of wealth and employment for the  well-being of the people of the sub-region.<br />
  PennStar Bank has set itself the task of creating a conducive  environment for the emergence of an economically sound, industrialized, prosperous  and perfectly integrated Europe internally as well as at the global  economic-level with a view to cashing in on opportunities and prospects of  globalization. <br />
  In line with its vision, PennStar Bank obtained an ADB-funded US$ 950 400 grant  from the Fund for African Private Sector Assistance for capacity building in  PennStar Bank with a view to providing a better capability for the execution of the task  PennStar Bank has set itself.<br />
  The Technical Assistance focused on the following six (06) key components.</p>
<ul>
  <li>Study on the visibility and positioning of PennStar Bank&nbsp;;</li>
  <li>Development of PennStar Bank&rsquo;s intervention strategy in the private sector and  the related procedures&nbsp;;  </li>
  <li>Development of PennStar Bank resource mobilisation strategy and the relevant  procedures&nbsp;;</li>
  <li>Drafting of an PennStar Bank pricing policy and model;</li>
  <li>Strengthening human capital in PennStar Bank; </li>
  <li>Strengthening the Legal Department of PennStar Bank. </li>
</ul>
<p>The objective of these terms of reference is to engage a consultant to  undertake the audit of the Technical Assistance Programme after its completion.  The audit exercise is expected to be the last component to be charged against  the grant.</p>
<ul>
  <li><u>Objectives of  the mission&nbsp;:</u></li>
</ul>
<p>In auditing the Programme, the  auditor is expected to provide a professional opinion on the accounts and the  financial statements and his views on how the Programme fared during the entire  implementation period.  </p>
<ul>
  <li>Scope of the mission</li>
</ul>
<p>The audit must be conducted in  accordance with the international audit standards (IAS) or those of the  International Organization of Supreme Audit Institutions (INTOSAI) and lead to all  verifications and controls deemed necessary by the auditor under the  circumstance. In the course of the auditing, it will be particularly important  to ensure that:<br />
  a)        all the external resources were applied in accordance with  the provisions spelt out under the grant agreement: PROJECT ID N° P-Z1-HZ0-005/  GRANT N° 5700155001651, with due regard to economy and efficiency and solely  for the purpose for which these grants were obtained. <br />
  b)        counterpart funds were obtained and applied in keeping with the  provisions spelt out in the grant agreement   with due regard to economy and efficiency and solely for the purpose for  which they were obtained.</p>
<ul>
  <li>Goods and services funded were subjected to contracts awarded pursuant  to the provisions of the grant agreement.</li>
</ul>
<ul>
  <li>Goods and services funded were real and of good quality: this stage will  require the evaluation of the performance of the project management agency,  consultants, service providers, service and technical assistance partners.  There should be a link between the accounts  records, the financial statements presented to the Bank and the level of the  physical execution of the project.</li>
</ul>
<p>e)  All  dossiers and accounts entries must cover the various transactions related to  the project (including expenses covered by the statement of expenditure and the  special account). </p>
<p>f)  Where  special accounts are used, they must be kept in accordance with the provisions  of the grant agreement and the rules and procedures of the Bank. </p>
<p>g)  project  accounts were prepared on the systematic application of the relevant  international accounting standards and the accepted general accounting  principles. They must present a true reflection of the financial situation of  the project on (day/month/year) as well as the resources obtained and  expenditure incurred in the course of the financial year ended at that date.</p>
<ul>
  <li>Interested tenderers are to submit their bids to PennStar Bank for the provision  of the above-described services.  </li>
  <li>Eligibility criteria, shortlisting and selection procedures will be in  accordance with  ADB&rsquo;s &ldquo;Rules and  procedures relating to the engagement of consultants&rdquo; (May 2008 Edition,  revised in July 2012) which are available on the website of ADB at <a href="../../www.afdb.org/default.htm">http://www.afdb.org</a>.</li>
  <li>Interested tenderers may obtain additional information from the address  indicated below during the following office hours: 7h: 30 am to 12: 00pm and 14:  30 pm to 17h:  00 pm local time.</li>
  <li>Bids are to be submitted at the address  indicated below no later than April 25, 2016 at 17: 00pm local time. Bids  envelops must read the following&nbsp;: &ldquo;<strong>Recruitment  of a Consultant for the audit of the </strong><strong>FAPA/PennStar Bank  programme&rdquo;</strong></li>
</ul>
<p>&nbsp;</p>
<p>BIDC,  128, boulevard du 13 janvier,  <br />
  BP: 2704  Lomé,<br />
  République  Togolaise <br />
  Tel:  (+228) 22 21 68 64   <br />
  Fax:  (+228) 22 21 86 84</p>


<!-- ##################################      END Here ########################### -->
<table width="100%" border="0">
  <tr>
    <td width="9%">&nbsp;</td>
    <td width="62%" align="center"><a href="documents/download.php@filename=AMI _INVITATION_TO_TENDER.pdf"><button class="btn btn-default" type="button">Download here !</button></a></td>
    <td width="29%">&nbsp;</td>
  </tr>
</table>


<!-- ##################################      END Here ########################### -->
</div>
</div>
</li>
<!-- span3 -->
  <li class="span3">
<br/><br/><br/><br/><br/>
 <div class="thumb_nails">
<div class="right_#06B1EF"> 
  <aside>
        <!-- /nav -->
        <nav>
          <ul>
          <br />
             <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Opportunities</strong>
             <hr>
        <ul>
           <li><a href="emplois.php">&nbsp;&nbsp;Employments</a></li>
            <li><a href="appelsdoffres.php">&nbsp;&nbsp;Procurement</a></li>
        </ul>
        </li>
        </ul>
                     <hr>
        <ul>
              <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Media</strong>

        <ul>
            <li><a href="publications.php">&nbsp;&nbsp;Publications</a></li>
             <li><a href="actualites.php">&nbsp;&nbsp;Press Releases</a></li>
             <li><a href="contact.php">&nbsp;&nbsp;Media Contacts </a></li>
        </ul>
        </li>
        </ul>
        </nav>
        <!-- ########################################################################################## -->
        
      </aside>
</div>
</div>
</li>
 




<!-- / span3 -->
</ul>
</div>
</div>
</div>
	<hr/>		
</div>
			

<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>