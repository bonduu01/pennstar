﻿<!DOCTYPE HTML>
<html>
<head>

<title>PennStar Bank | Higher Education Account</title>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="AUTHOR" CONTENT="Yachina DETE"> 
<META NAME="CREATION_DATE" CONTENT="06/07/2014">
<META NAME="DESCRIPTION" CONTENT="">
<META NAME="KEYWORDS" CONTENT="PennStar Bank">
<link rel="shortcut icon" href="assets/ico/bidc_favicon.ico">

<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-responsive.css" />

<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />

<link rel="stylesheet" type="text/css" href="assets/css/custom_2.css" />
<link href="assets/css/custom_2.css" rel="stylesheet" type="text/css" />

<link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/css3-mediaqueries.js"></script>
<script type="text/javascript" src="assets/js/default.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>


<script src="assets/js/css3-mediaqueries.js" type="text/javascript"></script>
<script type='text/javascript' src="cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/modernizr.min.js"></script>



<script src="assets/js/css3-mediaqueries.js"></script>
<script type='text/javascript' src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../../html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Icons -->

    <!--[if lt IE 8]>
        <link href="scripts/icons/general/stylesheets/general_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="scripts/icons/social/stylesheets/social_foundicons_ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
   
    <!--[if IE 7]>
        <link rel="stylesheet" href="scripts/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
<link href='../../fonts.googleapis.com/css@family=Open+Sans_3A400,300,700' rel='stylesheet' type='text/css'>
<link href='../../fonts.googleapis.com/css@family=Open+Sans+Condensed_3A700bold,300' rel='stylesheet' type='text/css'>

</head>
<body>
<!-- header -->
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
            <div class="row-fluid">
<div class="logos_box"><div class="logos"><img src="assets/logo_450.jpg" width="450" height="103" alt="PennStar Bank Logo"></div></div>

<div class="lang_box"> 
  <div class="lang">
<a href="#">Fran&ccedil;ais </a>| <a href="index.php"><strong>English </strong> </a>| <a href="#">Portugu&eacute;s</a>
  </div>
   <br class="clear" />
<div class="searche_bow">
<form class="form-search" action="#" method="get">
<input type="text" class="input-medium search-query" name="query" id="query" size="20" value="" action="../search/include/js_suggest/suggest.php" columns="2" autocomplete="off" delay="1500">
      <button type="submit" class="btn btn-info ">Search</button>
      
<input type="hidden" name="search" value="1">
 </form>
    
</div>
</div>

</div>
</div>
</div><!-- / header -->

<!--  navbar -->

<div id="topmenu">
<div id="divBox" class="container">
    <div class="divPanel notop nobottom">
             <div class="row-fluid">
                <div class="span12">
           
                    <div id="divMenuRight" class="pull-left">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-warning" data-toggle="collapse" data-target=".nav-collapse">
                            Menu <i class="fa fa-chevron-down fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
         
      <li><a href="index.php">Home</a></li>
        <li><a href="apropos.php">About Us</a></li>

        
        <li><a href="fondspeciaux.php">Special Funds</a></li>
        <li  class="active"><a href="emplois.php">Jobs</a></li>
         <li><a href="appelsdoffres.php">Procurement</a></li>

                            
                            
                            </ul>
                            </div>
                    </div>
                    </div>
               

                </div>
            </div>

            
    </div>

</div>
</div>
<div class="row"><br></div> 
      




 <!-- /.navbar -->

  
<!-- #banner -->
<!-- /#banner -->

<!--Introduction-->


<!-- /Introduction -->

<!-- breadcrumb -->
<div class="container_intern">
<div class="row">
<ul class="ariane">
<li><a href="index.php"><i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#000"></i></a></li>			<li>Resources</li>
            <li>Higher Education Account</li>
		</ul>
</div>
</div>


<!-- /breadcrumb -->
<div class="row"><br> </div>
<div id="divBox" class="container">
<div class="business-header">
<div class="container">
<div class="row-fluid">
<div class="col-lg-12">
<div id="slideshow">
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_1.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_2.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_3.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_4.jpg" alt="icon" width="960" height="300" border="0" /></a></div>
<div class="slider-item"><a href="#"><img src="slideshow/banner_en_5.jpg" alt="icon" width="960" height="300" border="0" /></a></div>

</div>
</div>
</div>
</div>
</div>
<!--Edit Portfolio Content Area here-->			
<div class="row-fluid">
<div class="span12">
<div class="row-fluid">
<ul class="thumb_nails">
<li class="span9">
<div class="thumb_nails">
<div class="caption">
<h1>Higher Education Account</h1>
<h3>Here's what you get</h3>
<hr />
<p><h4>Fee-free overdraft available</h4>
Apply for an overdraft of up to $3,000 (subject to status) with fee-free amounts available depending on when you graduated. 
You can find more information about the overdraft and the charges below.</p>
<p><h4>Representative example:</h4>

For your 1st year after graduation:
Fee-free overdraft of up to $3000 (subject to application and status) while you stay within that limit.
Lower overdraft fee-free amount applies in 2nd and 3rd year after graduation.</p>
<p><h4>PennStar Bank Blue Rewards</h4>
Get an annual loyalty reward of $84 plus $60 on your PennStar Bank residential mortgage and $36 on your PennStar Bank home and contents insurance. Start earning rewards with a monthly $3 fee, $800 paid into your account and 2 Direct Debits paid out every month</p>
<p><h4>Save money and earn cashback with our partner rewards</h4>

Earn cashback and save money with selected brands like Apple, Nike, Amazon, Oasis, Boots and hungryhouse. You can also earn cashback when you book travel with Expedia.</p>

<p><h4>Make payments on the go</h4>


With Apple Pay2 or Contactless Mobile3, you can use your mobile phone to pay with your PennStar Bank debit card or Barclaycard wherever you see the Apple Pay or contactless logo.</p>

<p><h4>7-day switch</h4>
It’s safe and simple – we guarantee to move all your old payments to your new account within just 7 working days.</p>

<!-- ##################################      END Here ########################### -->
</div>
</div>
</li>
<!-- span3 -->
  <li class="span3">
<br/><br/><br/><br/><br/>
 <div class="thumb_nails">
<div class="right_#06B1EF"> 
  <aside>
        <!-- /nav -->
        <nav>
          <ul>
          <br />
             <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Opportunities</strong>
             <hr>
        <ul>
           <li><a href="emplois.php">&nbsp;&nbsp;Employments</a></li>
            <li><a href="appelsdoffres.php">&nbsp;&nbsp;Procurement</a></li>
        </ul>
        </li>
        </ul>
                     <hr>
        <ul>
              <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Media</strong>

        <ul>
            <li><a href="publications.php">&nbsp;&nbsp;Publications</a></li>
             <li><a href="actualites.php">&nbsp;&nbsp;Press Releases</a></li>
             <li><a href="contact.php">&nbsp;&nbsp;Media Contacts </a></li>
        </ul>
        </li>
        </ul>
        </nav>
        <!-- ########################################################################################## -->
        
      </aside>
</div>
</div>
</li>
 




<!-- / span3 -->
</ul>
</div>
</div>
</div>
	<hr/>		
</div>
			

<!--/End Portfolio Content Area-->

<!-- footer -->
<div class="business-footer">
<div class="container">
<div id="divFooter">



<div class="row-fluid">
<div class="span3" id="footerArea1">

<h3>About us</h3>
<p><a href="apropos.php">Mission</a></p>
<p><a href="apropos.php">Vision</a></p>
<p><!--<a href="organisation.php">Organization</a>--></p>
<p><!--<a href="management.php">Management</a>--></p>
</div>

<div class="span3" id="footerArea2">

<h3>Resources</h3> 
<p><a href="newsdetails.php">Student Additions Account</a></p>
<p><a href="emplois.php">Job Opportunities</a></p>
<p><a href="studentdetails.php">Student Additions Account (international)</a></p>

</div>
<div class="span3" id="footerArea3">



</div>
<div class="span3" id="footerArea4">
<!--<h3><a href="contact.php">Contacts</a></h3>-->  
<i class="fa fa-home fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">South African Address:</span>-->
<br />
<!--38 Long Street,<br />-->
<!--Cape Town 8000<br />-->   
<i class="fa fa-phone fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">Tel: +27815394120</span><br /><span class="courriel">Tel: +27218246758</span>-->
<br />
<i class="fa fa-print fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">US Tel: +12815492066 </span>-->
<br />
<i class="fa fa-envelope fa-fw fa-3x" style="font-size:larger; color:#fff"></i>
<!--<span class="courriel">&nbsp;<a href="mailto:customercare@PennStar Bank.bnkserv.com" title="Email">email:customercare@PennStar Bank.bnkserv.com</a></span>-->                                       
</div>
</div>


</div>

</div>
</div>
<!-- copyright -->
<div class="business-copyright">
<div class="container">
<div class="divPanel">
<div class="row-fluid">
<div class="span3">
<div class="site_map">	
<li style="list-style:none"><a rel="facebox" href="admin/login.php" target="_blank"><i class="fa fa-lock"></i>&nbsp;Staff login</a></li>
<li style="list-style:none"><a href="plandusite.php"><i class="fa fa-tags"></i>&nbsp;Site map</a></li>
</div>
</div>
<div class="span3">
<div class="suivez_moi">Follow us >>></div>
</div>
<div class="span3">
<a href="#"><img src="assets/web2/png/glyphicons_social_30_facebook.png" alt="facebook" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_31_twitter.png" alt="twitter" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_02_google_plus.png" alt="google" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_17_linked_in.png" alt="linked_in" ></a>
<a href="#">&nbsp;&nbsp;<img src="assets/web2/png/glyphicons_social_37_rss.png" alt="rss" ></a>
</div>
<div class="span3">
<div class="droits_de_reserves">
<p> Copyright &copy; 2019 PennStar Bank, All right reserved.</p>
</div>
</div>
</div>

</div>
</div>
</div>
<!-- /copyright --><!-- / footer -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="../../https@ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../code.jquery.com/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="JavaScript" src="assets/js/jquery-1.6.1.min.js"></script>
<script language="JavaScript" src="assets/js/js.js"></script>
<script type="text/javascript" src="assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="assets/js/js_1.js"></script>

<script src="assets/js/default.js" type="text/javascript"></script>

 <!--  ##################################  -->

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>
		<script type="text/javascript">
			$('#ca-container').contentcarousel();
		</script>


    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>