/*** ---------------------------------------------------------------------------------------------------------------------------------------------------------*/
var slidePos = 0;

// shortcut for dom getelementbyid
function get_id (str_el) { return document.getElementById(str_el); }
/*
=================================================
== SLIDESOW CODE BY MICHAEL OCANSEY 
== MIKE@KURSORSOLUTIONS.COM
== basic slideshow animation that requires jQuery
== October, 02, 2011
==================================================
*/
// this is where to list the images you want to use for the slideshow
json = {"pictures" : [{"url":"slideshow/image2.jpg"},{"url":"slideshow/image3.jpg"},{"url":"slideshow/image4.jpg"},{"url":"slideshow/image5.jpg"},{"url":"slideshow/image6.jpg"},{"url":"slideshow/image7.jpg"},{"url":"slideshow/image8.jpg"},{"url":"slideshow/image9.jpg"},{"url":"slideshow/image10.jpg"},{"url":"slideshow/image11.jpg"},{"url":"slideshow/image12.jpg"}]};
// get total number of pictures
var totalPics = json.pictures.length;
function changePix() {
$("#mascot").attr('style','background: url('+json.pictures[slidePos].url+') no-repeat').fadeTo('fast',0.3).fadeTo('slow',1);
slidePos++;
if(slidePos >= totalPics) slidePos = 0;
}
//initialise the slideshow // make sure jQuery is included in your html page
$(document).ready(function() { setInterval("changePix()",5000); })